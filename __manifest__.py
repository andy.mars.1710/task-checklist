# -*- coding: utf-8 -*-
{
    'name': "Task Check List",

    'summary': """
        Task Check List
        Develop by Berryhaus""",

    'description': """
        Project task check list
    """,

    'author': "Berryhaus",
    'website': "https://berryhaus.com",
    "price": 100,
    "currency": "EUR",
    'category': 'Extra Tools',
    'version': '0.1',
    'license': 'OPL-1',
    'depends': ['base', 'project'],
    'images': ['static/images/0.png'],
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
    ],
}
